<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Categories extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('categories_model');
	}

	function category_data(){
		$data=$this->categories_model->getCategories();
		echo json_encode($data);
	}

	function save(){
		$data=$this->categories_model->add_category();
		echo json_encode($data);
	}
}
