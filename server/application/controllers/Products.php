<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('products_model');
	}

	public function index()
	{
		$data['title'] = "Shopping list";
		$data['products'] = $this->products_model->getProducts();


		$this->load->view('templates/header', $data);
		$this->load->view('products/index', $data);
	}

	function products_data(){
		$data=$this->products_model->getProducts();
		echo json_encode($data);
	}

	function products_filter(){
		$data=$this->products_model->filter();
		echo json_encode($data);
	}

	function save(){
		$data=$this->products_model->add_product();
		echo json_encode($data);
	}

	function update(){
		$data=$this->products_model->mark_as_bought();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->products_model->delete_product();
		echo json_encode($data);
	}
}
