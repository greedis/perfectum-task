<?php

class Categories_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function getCategories($slug = false)
	{
		if ($slug === false){
			$query = $this->db->get('categories');
			return $query->result_array();
		}
		$query = $this->db->get_where('categories', array('category_id' => $slug));
		return $query->result_array();
	}

	public function add_category()
	{
		$data = array(
			'name'  => $this->input->post('name'),
		);
		$query = $this->db->insert('categories',$data);
		return $query;
	}
}
