<?php

class Products_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function getProducts($slug = false)
	{
		$this->db->join('categories', 'categories.id = products.category_id');
		$this->db->select('products.id, product, name, count, status, created_at');
		$query = $this->db->get('products');
		return $query->result_array();
	}

	public function filter()
	{
		$col = $this->input->post('col');
		$value = $this->input->post('value');
		$this->db->join('categories', 'categories.id = products.category_id');
		$this->db->where($col, $value);
		$this->db->select('products.id, product, name, count, status, created_at');
		$query = $this->db->get('products');
		return $query->result_array();
	}

	public function add_product()
	{
		$data = array(
			'product' => $this->input->post('product'),
			'category_id' => $this->input->post('category_id'),
			'count' => $this->input->post('count'),
			'created_at' => date('Y-m-d'),
		);
		$query = $this->db->insert('products', $data);
		return $query;
	}

	public function mark_as_bought()
	{
		$id = $this->input->post('id');
		$status = $this->input->post('status');

		$this->db->set('status', $status);
		$this->db->where('id', $id);
		$result = $this->db->update('products');
		return $result;
	}

	function delete_product()
	{
		$id = $this->input->post('id');
		$this->db->where('id', $id);
		$result = $this->db->delete('products');
		return $result;
	}
}
