<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="col-md-12">
				<h1>Shopping
					<small>List</small>
					<div class="float-right"><a href="javascript:void(0);" class="btn btn-primary"
												data-bs-toggle="modal" data-bs-target="#Modal_Add"><span
									class="fa fa-plus"></span> Add New</a>
						<a href="javascript:void(0);" class="btn btn-primary"
						   data-bs-toggle="modal" data-bs-target="#Modal_Category"><span
									class="fa fa-plus"></span> Add Category</a>
						<a href="javascript:void(0);" class="btn btn-primary"
						   data-bs-toggle="modal" data-bs-target="#Modal_Filter_Category"><span
									class="fa fa-plus"></span> Filter by category</a>
						<a href="javascript:void(0);" class="btn btn-primary"
						   data-bs-toggle="modal" data-bs-target="#Modal_Filter_Status"><span
									class="fa fa-plus"></span> Filter by status</a>
						<a href="javascript:void(0);" class="btn btn-primary" id="resetFilters">
							<span class="fa fa-plus"></span> Reset filters</a></div>
				</h1>
			</div>

			<table class="table table-striped" id="mydata">
				<thead>
				<tr>
					<th>Product Name</th>
					<th>Category</th>
					<th>Count</th>
					<th>Status</th>
					<th>Date</th>
				</tr>
				</thead>
				<tbody id="show_data">

				</tbody>
			</table>
		</div>
	</div>
</div>
<form>
	<div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New Product</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-md-2 col-form-label">Product Name</label>
						<div class="col-md-10">
							<input type="text" name="product" id="product" class="form-control"
								   placeholder="Product Name" required>
						</div>
					</div>
					<div class="input-group mb-3" style="margin-top: 20px">
						<label class="input-group-text" for="inputGroupSelect01">Category</label>
						<select class="form-select" id="categoryId">

						</select>
					</div>
					<div class="form-group row">
						<label class="col-md-2 col-form-label">Count</label>
						<div class="col-md-10">
							<input type="text" name="count" id="count" class="form-control" placeholder="Count"
								   required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" id="btn_save" class="btn btn-primary">Add</button>
				</div>
			</div>
		</div>
	</div>
</form>
<form>
	<div class="modal fade" id="Modal_Filter_Category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Filter by category</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="input-group mb-3" style="margin-top: 20px">
						<label class="input-group-text" for="inputGroupSelect01">Category</label>
						<select class="form-select" id="categoryIdF">

						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" id="btn_filter_category" class="btn btn-primary">Filter</button>
				</div>
			</div>
		</div>
	</div>
</form>
<form>
	<div class="modal fade" id="Modal_Filter_Status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New Product</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="input-group mb-3" style="margin-top: 20px">
						<label class="input-group-text" for="inputGroupSelect01">Status</label>
						<select class="form-select" id="status">
							<option name="status" value="not_bought">Not bought</option>
							<option name="status" value="bought">Bought</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" id="btn_filter_by_status" class="btn btn-primary">Filter</button>
				</div>
			</div>
		</div>
	</div>
</form>
<form>
	<div class="modal fade" id="Modal_Category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New Category</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-md-2 col-form-label">Category Name</label>
						<div class="col-md-10">
							<input type="text" name="name" id="name" class="form-control"
								   placeholder="Category Name" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" id="btn_save_category" class="btn btn-primary">Add</button>
				</div>
			</div>
		</div>
	</div>
</form>
<form>
	<div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Delete Product</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<strong>Are you sure to delete this record?</strong>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="id" class="form-control">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
					<button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
				</div>
			</div>
		</div>
	</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
		crossorigin="anonymous"></script>
<script src="<?= base_url() . 'js/jquery-3.6.1.min.js' ?>"></script>
<script>
	$(document).ready(function () {
		show_product();
		show_categories();

		function show_product(filter = '') {
			$.ajax({
				type: 'ajax',
				url: '<?=site_url('load_products/')?>' + filter,
				async: true,
				dataType: 'json',
				success: function (data) {
					console.log(data);
					let html = '';
					let i;
					let len = data.length;
					for (i = 0; i < len; i++) {
						if (data[i]['status'] === 'not_bought') {
							data[i]['status'] = 'not bought';
						}
						html += '<tr>' +
							'<td>' + data[i]['product'] + '</td>' +
							'<td>' + data[i]['name'] + '</td>' +
							'<td>' + data[i]['count'] + '</td>' +
							'<td>' + data[i]['status'] + '</td>' +
							'<td>' + data[i]['created_at'] + '</td>' +
							'<td style="text-align:right;">';
						if (data[i]['status'] === 'not bought') {
							html += '<a style="margin-right: 5px" href="javascript:void(0);" class="btn btn-info btn-sm item_bought" data-id="' + data[i]['id'] + '">Mark as bought</a>';

						}
						html += '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="' + data[i]['id'] + '">Delete</a>' +
							'</tr>';
					}
					$('#show_data').html(html);
				}

			});
		}

		function show_categories(filter = '') {
			$.ajax({
				type: 'ajax',
				url: '<?=site_url('load_categories/')?>' + filter,
				async: true,
				dataType: 'json',
				success: function (data) {
					console.log(data);
					let html = '<option selected>Choose category</option>';
					let i;
					let len = data.length;
					for (i = 0; i < len; i++) {
						html += `<option name="category_id" value="${data[i]['id']}">${data[i]['name']}</option>`;
					}
					$('#categoryId').html(html);
					$('#categoryIdF').html(html);
				}

			});
		}

		$(document).on('click', '#btn_save', function (e) {
			let product = $('#product').val();
			let category_id = $('#categoryId').val();
			let count = $('#count').val();
			console.log(product, category_id, count);
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('save_product')?>",
				dataType: "JSON",
				data: {product: product, category_id: category_id, count: count},
				success: function (data) {
					$('[name="product"]').val("");
					$('[name="category_id"]').val("");
					$('[name="count"]').val("");
					$('#Modal_Add').modal('hide');
					show_product();
				}
			});
			return false;
		});
		$('#btn_save_category').on('click', function (e) {
			let name = $('#name').val();
			console.log(name);
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('categories/save')?>",
				dataType: "JSON",
				data: {name: name},
				success: function (data) {
					$('[name="name"]').val("");
					$('#Modal_Category').modal('hide');
					show_categories();
				}
			});
			return false;
		});
		$('#show_data').on('click', '.item_delete', function () {
			let id = $(this).data('id');

			$('#Modal_Delete').modal('show');
			$('[name="id"]').val(id);
		});

		$('#btn_delete').on('click', function () {
			let id = $('#id').val();
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('products/delete')?>",
				dataType: "JSON",
				data: {id: id},
				success: function (data) {
					$('[name="id"]').val("");
					$('#Modal_Delete').modal('hide');
					show_product();
				}
			});
			return false;
		});

		$('#show_data').on('click', '.item_bought', function () {
			let id = $(this).data('id');
			let status = 'bought';

			$.ajax({
				type: "POST",
				url: "<?php echo site_url('products/update')?>",
				dataType: "JSON",
				data: {id: id, status: status},
				success: function (data) {
					$('[name="id"]').val("");
					show_product();
				}
			});
			return false;
		});
		$('#btn_filter_category').on('click', function (e) {
			let category_id = $('#categoryIdF').val();
			let col = 'category_id';
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('products/products_filter')?>",
				dataType: "JSON",
				data: {col: col, value: category_id},
				success: function (data) {
					$('#Modal_Filter_Category').modal('hide');
					let html = '';
					let i;
					let len = data.length;
					for (i = 0; i < len; i++) {
						if (data[i]['status'] === 'not_bought') {
							data[i]['status'] = 'not bought';
						}
						html += '<tr>' +
							'<td>' + data[i]['product'] + '</td>' +
							'<td>' + data[i]['name'] + '</td>' +
							'<td>' + data[i]['count'] + '</td>' +
							'<td>' + data[i]['status'] + '</td>' +
							'<td>' + data[i]['created_at'] + '</td>' +
							'<td style="text-align:right;">';
						if (data[i]['status'] === 'not bought') {
							html += '<a style="margin-right: 5px" href="javascript:void(0);" class="btn btn-info btn-sm item_bought" data-id="' + data[i]['id'] + '">Mark as bought</a>';

						}
						html += '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="' + data[i]['id'] + '">Delete</a>' +
							'</tr>';
					}
					$('#show_data').html(html);
				}
			});
			return false;
		});
		$('#btn_filter_by_status').on('click', function (e) {
			let status = $('#status').val();
			let col = 'status';
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('products/products_filter')?>",
				dataType: "JSON",
				data: {col: col, value: status},
				success: function (data) {
					$('#Modal_Filter_Status').modal('hide');
					let html = '';
					let i;
					let len = data.length;
					for (i = 0; i < len; i++) {
						if (data[i]['status'] === 'not_bought') {
							data[i]['status'] = 'not bought';
						}
						html += '<tr>' +
							'<td>' + data[i]['product'] + '</td>' +
							'<td>' + data[i]['name'] + '</td>' +
							'<td>' + data[i]['count'] + '</td>' +
							'<td>' + data[i]['status'] + '</td>' +
							'<td>' + data[i]['created_at'] + '</td>' +
							'<td style="text-align:right;">';
						if (data[i]['status'] === 'not bought') {
							html += '<a style="margin-right: 5px" href="javascript:void(0);" class="btn btn-info btn-sm item_bought" data-id="' + data[i]['id'] + '">Mark as bought</a>';

						}
						html += '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="' + data[i]['id'] + '">Delete</a>' +
							'</tr>';
					}
					$('#show_data').html(html);
				}
			});
			return false;
		});
		$('#resetFilters').on('click', function (e) {
			show_product();
		});
	});
</script>
</body>
</html>

